import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import {routing} from "./app.routes";
import {AppComponent}  from './app.component';
import {InstructorsComponent} from "./instructors/instructors.component";
import {RootComponent} from './root/root.component';
import {InstructorsService} from "./instructors/instructors.service";
import {FooterComponent} from "./core/footer.component";
import {NavbarComponent} from "./core/navbar.component";
import {InstructorEditComponent} from "./instructors/instructor-edit.component";
import {InstructorAddComponent} from "./instructors/instructor-add.component";
import {LoginComponent} from "./login/login.component";
import {LogoutComponent} from "./login/logout.component";
import {
  ScheduleModule,
  DialogModule,
  CalendarModule,
  ToggleButtonModule,
  DropdownModule,
  TabViewModule,
  SelectButtonModule,
  MultiSelectModule
} from 'primeng/primeng';
import {AuthService} from "./login/auth.service";
import {AuthGuardService} from "./auth-guard.service";
import {RatesComponent} from "./rates/rates.component";
import {RatesService} from "./rates/rates.service";
import {RateEditComponent} from "./rates/rate-edit.component";
import {RateAddComponent} from "./rates/rate-add.component";
import {KeysComponent} from "./keys/keys.component";
import {KeysService} from "./keys/keys.service";
import {KeyEditComponent} from "./keys/key-edit.component";
import {KeyAddComponent} from "./keys/key-add.component";
import {ClientEditComponent} from "./clients/client-edit.component";
import {ClientAddComponent} from "./clients/client-add.component";
import {ClientsService} from "./clients/clients.service";
import {ClientsComponent} from "./clients/clients.component";
import {GymsService} from "./rates/gym.service";


@NgModule({
  imports: [BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ScheduleModule,
    DialogModule,
    CalendarModule,
    ToggleButtonModule,
    TabViewModule,
    DropdownModule,
    SelectButtonModule,
    MultiSelectModule,
    Ng2Bs3ModalModule,
    routing],
  declarations: [AppComponent,
    InstructorsComponent,
    FooterComponent,
    NavbarComponent,
    RootComponent,
    InstructorEditComponent,
    InstructorAddComponent,
    LoginComponent,
    LogoutComponent,
    RateEditComponent,
    RateAddComponent,
    RatesComponent,
    KeyEditComponent,
    KeyAddComponent,
    KeysComponent,
    ClientEditComponent,
    ClientAddComponent,
    ClientsComponent],
  providers: [InstructorsService, AuthService, AuthGuardService, RatesService, KeysService, ClientsService, GymsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
