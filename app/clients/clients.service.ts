import {Injectable, EventEmitter} from '@angular/core';
import {Client} from "./client.class";
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/Rx';
import appGlobals = require('../app.global'); //<==== config

@Injectable()
export class ClientsService {
  clients: Client[] = [];
  clientsUpdated = new EventEmitter<Client[]>();
  headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('id_token')
  });

  constructor(private http: Http) {
  }

  getClients() {
    return this.clients;
  }

  getClientById(id: number) {
    for (var client of this.clients) {
      if (client.id === id) {
        return client;
      }
    }
    return null;
  }

  deleteClient(client: Client) {
    const id = client.id;

    this.clients.splice(this.clients.indexOf(client), 1);
    this.http.delete(appGlobals.rest_server + 'clients/' + id, {headers: this.headers}).subscribe((res) => {
    });
  }

  editClient(oldClient: Client, newClient: Client) {
    this.clients[this.clients.indexOf(oldClient)] = newClient;
    const body = JSON.stringify(newClient);
    return this.http.put(appGlobals.rest_server + 'clients?nocache=' + new Date().getTime(), body, {headers: this.headers}).subscribe((res) => {
      this.clientsUpdated.emit(this.clients);
    });
  }

  addClient(client: Client) {
    console.log(client);
    const body = JSON.stringify(client);
    return this.http.post(appGlobals.rest_server + 'clients?nocache' + new Date().getTime(), body, {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe((data) => {
        client.id = data[0].id;
        this.clients.push(client);
        this.clientsUpdated.emit(this.clients);
      });
  }

  fetchClients() {
    return this.http.get(appGlobals.rest_server + 'clients?nocache=' + new Date().getTime(), {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe(
        (data: Client[]) => {
          this.clients = data;
          this.clientsUpdated.emit(this.clients);
        }
      );
  }
}
