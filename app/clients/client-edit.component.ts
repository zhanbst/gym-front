import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {ClientsService} from "./clients.service";
import { Subscription } from 'rxjs/Rx';
import {Client} from "./client.class";

@Component({
    moduleId: module.id,
    selector: 'tsa-client-edit',
    templateUrl: 'client-edit.component.html'
})

export class ClientEditComponent implements OnInit, OnDestroy {
  clientForm: FormGroup;
  private subscription: Subscription;
  clientId: number;
  client: Client;

  constructor(private route: ActivatedRoute,
              private clientsService: ClientsService,
              private formBuilder: FormBuilder,
              private router: Router) {}

  ngOnInit() {
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.clientId = +params['id'];
          this.client = this.clientsService.getClientById(this.clientId);
        }
      }
    );
    this.initForm();
  }

  private initForm() {
    this.clientForm = this.formBuilder.group({
      id: [this.client.id, Validators.required],
      name: [this.client.name, Validators.required],
      address: [this.client.address, Validators.required],
      email: [this.client.email, Validators.required],
      phone: [this.client.phone, Validators.required]
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onCancel() {
    this.router.navigate(['/clients']);
  }

  onSubmit() {
    const newClient = this.clientForm.value;
    this.clientsService.editClient(this.client, newClient);
    this.router.navigate(['/clients']);
  }
}
