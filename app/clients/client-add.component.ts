import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import { ClientsService } from "./clients.service";
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Client } from "./client.class";
import {Rate} from '../rates/rate.class';
import {Gym} from '../rates/gym.class';
import {Instructor} from '../instructors/instructor.class';
import {RatesService} from '../rates/rates.service';
import {GymsService} from '../rates/gym.service';
import {InstructorsService} from '../instructors/instructors.service';


@Component({
    moduleId: module.id,
    selector: 'tsa-client-add',
    templateUrl: 'client-add.component.html'
})

export class ClientAddComponent implements OnInit {
  modal:ModalComponent;
  clientForm: FormGroup;
  client: Client;
  rates: Rate[];
  gyms: Gym[];
  instructors: Instructor[];
  selectedRates: number[];
  selectedInstructors: number[];
  startdatetime: Date;
  displayRatesDialog: boolean = false;

  constructor(private clientsService: ClientsService,
              private instructorsService: InstructorsService,
              private ratesService: RatesService,
              private gymsService: GymsService,
              private router: Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.ratesService.ratesUpdated.subscribe(
      (rates: Rate[]) => {

        rates.forEach(v=>{

        });
        this.rates = rates;
      }
    );
    this.gymsService.gymsUpdated.subscribe(
      (gyms: Gym[]) => {
        this.gyms = gyms;
      }
    );
    this.instructorsService.instructorsUpdated.subscribe(
      (instructors: Instructor[]) => {
        this.instructors = instructors;
      }
    );
    this.ratesService.fetchRates();
    this.rates = this.ratesService.getRates();
    this.instructorsService.fetchInstructors();
    this.instructors = this.instructorsService.getInstructors();
    this.gymsService.fetchGyms();
    this.gyms = this.gymsService.getGyms();

    console.log(this.rates);


    this.clientForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      selectRates: [0],
      selectInstructors:[0],
      startdatetime:['']
    });
  }

  onChangeRates(){
    console.log(this.selectedRates);
  }

  onCancel() {
    this.router.navigate(['/clients']);
  }


  onSubmit() {
    this.clientsService.addClient(this.clientForm.value);
    this.router.navigate(['/clients']);
  }
}
