import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ClientsService} from "./clients.service";
import {Client} from "./client.class";

@Component({
  moduleId: module.id,
  selector: 'tsa-clients',
  templateUrl: 'clients.component.html'
})

export class ClientsComponent implements OnInit {
  clients: Client[];

  constructor(public clientsService: ClientsService,
              private router: Router) {
  }

  ngOnInit() {
    this.clientsService.clientsUpdated.subscribe(
      (clients: Client[]) => {
        this.clients = clients;
      }
    );
    this.clients = this.clientsService.getClients();
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    else if (this.clients.length === 0) {
      this.clientsService.fetchClients();
    }

  }

  onDeleteClient(client: Client) {
    this.clientsService.deleteClient(client);
    this.router.navigate(['/clients']);
  }

  onEditClient(client: Client) {
    this.router.navigate(['/clients', client.id, 'edit']);
  }

  onAddClient() {
    this.router.navigate(['/clients/new']);
  }
}
