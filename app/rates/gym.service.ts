/**
 * Created by zhanik on 11.11.16.
 */
import {Injectable, EventEmitter} from '@angular/core';
import {Gym} from "./gym.class";
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/Rx';
import appGlobals = require('../app.global'); //<==== config

@Injectable()
export class GymsService {
  gyms: Gym[] = [];
  gymsUpdated = new EventEmitter<Gym[]>();


  headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('id_token')
  });

  constructor(private http: Http) {
  }

  getGyms() {
    return this.gyms;
  }

  getGymById(id: number) {
    for (var gym of this.gyms) {
      if (gym.id === id) {
        return gym;
      }
    }
    return null;
  }

  deleteGym(gym: Gym) {
    const id = gym.id;

    this.gyms.splice(this.gyms.indexOf(gym), 1);
    this.http.delete(appGlobals.rest_server + 'gyms/' + id, {headers: this.headers}).subscribe((res) => {
    });
  }

  editGym(oldGym: Gym, newGym: Gym) {
    this.gyms[this.gyms.indexOf(oldGym)] = newGym;
    const body = JSON.stringify(newGym);
    return this.http.put(appGlobals.rest_server + 'gyms?nocache=' + new Date().getTime(), body, {headers: this.headers}).subscribe((res) => {
      this.gymsUpdated.emit(this.gyms);
    });
  }



  addGym(name: string) {
    let gym:Gym = {name: name, id:null};
    const body = {name: name};
    return this.http.post(appGlobals.rest_server + 'gyms?nocache' + new Date().getTime(), body, {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe((data) => {
      console.log(data);
        gym.id = data[0].id;
        gym.name = name;
        this.gyms.push(gym);
        this.gymsUpdated.emit(this.gyms);
      });
  }

  fetchGyms() {
    return this.http.get(appGlobals.rest_server + 'gyms?nocache=' + new Date().getTime(), {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe(
        (data: Gym[]) => {
          this.gyms = data;
          this.gymsUpdated.emit(this.gyms);
        }
      );
  }
}
