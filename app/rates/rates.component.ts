import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RatesService} from "./rates.service";
import {Rate} from "./rate.class";

@Component({
  moduleId: module.id,
  selector: 'tsa-rates',
  templateUrl: 'rates.component.html'
})

export class RatesComponent implements OnInit {
  rates: Rate[];

  constructor(public ratesService: RatesService,
              private router: Router) {
  }

  ngOnInit() {
    this.ratesService.ratesUpdated.subscribe(
      (rates: Rate[]) => {
        this.rates = rates;
      }
    );
    this.rates = this.ratesService.getRates();
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    else if (this.rates.length === 0) {
      this.ratesService.fetchRates();
    }

  }

  onDeleteRate(rate: Rate) {
    this.ratesService.deleteRate(rate);
    this.router.navigate(['/rates']);
  }

  onEditRate(rate: Rate) {
    this.router.navigate(['/rates', rate.id, 'edit']);
  }

  onAddRate() {
    this.router.navigate(['/rates/new']);
  }
}
