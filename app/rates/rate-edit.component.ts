import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import { RatesService } from "./rates.service";
import { Subscription } from 'rxjs/Rx';
import {Gym} from './gym.class';
import {GymsService} from './gym.service';
import {Rate} from "./rate.class";

@Component({
    moduleId: module.id,
    selector: 'tsa-instructor-edit',
    templateUrl: 'rate-edit.component.html'
})

export class RateEditComponent implements OnInit, OnDestroy {
  rateForm: FormGroup;
  private subscription: Subscription;
  rateId: number;
  rate: Rate;
  nameGym: string = '';
  gyms: Gym[];
  gym: Gym;
  starttime: Date;
  endtime: Date;
  weekDays: SelectItem[];
  weekdays_: Array<any> = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
  selectedDays: number[] = [];

  constructor(private route: ActivatedRoute,
              private ratesService: RatesService,
              private gymsService: GymsService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.weekDays = [];
    for(let i=0; i<7; i++){
      this.weekDays.push({label:this.weekdays_[i],value:i});
    }

  }

  ngOnInit() {
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.rateId = +params['id'];
          this.rate = this.ratesService.getRateById(this.rateId);
          let starttime = this.rate.starttime.split(':').map(v=>parseInt(v));
          let endtime = this.rate.endtime.split(':').map(v=>parseInt(v));
          this.starttime = new Date(0,0,0,starttime[0],starttime[1],0);
          this.endtime = new Date(0,0,0,endtime[0],endtime[1],0);
        }
      }
    );
    this.gymsService.gymsUpdated.subscribe(
      (gyms: Gym[]) => {
        this.gyms = gyms;
      }
    );
    this.gymsService.fetchGyms();
    this.gyms = this.gymsService.getGyms();

    this.initForm();
  }

  private initForm() {

    this.selectedDays = Array.isArray(this.rate.weekdays)?this.rate.weekdays:JSON.parse(this.rate.weekdays);
    console.log(this.rate);
    console.log(this.selectedDays,this.starttime,this.endtime);
    this.rateForm = this.formBuilder.group({
      id: [this.rate.id, Validators.required],
      name: [this.rate.name, Validators.required],
      price: [this.rate.price, Validators.required],
      gymid: [this.rate.gymid, Validators.required],
      training: [this.rate.training, Validators.required],
      starttime: [this.starttime,Validators.required],
      endtime: [this.endtime,Validators.required],
      weekdays: [this.selectedDays, Validators.required],
      enddate: [this.rate.enddate, Validators.required],
      count: [this.rate.count, Validators.required],
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onCancel() {
    this.router.navigate(['/rates']);
  }

  onSubmit() {
    const newRate = this.rateForm.value;
    this.ratesService.editRate(this.rate, newRate);
    this.router.navigate(['/rates']);
  }
}
