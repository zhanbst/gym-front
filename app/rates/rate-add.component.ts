import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, Validator} from '@angular/forms';
import {SelectItem} from 'primeng/primeng';
import { Router } from '@angular/router';
import { RatesService } from "./rates.service";
import {Gym} from './gym.class';
import {GymsService} from './gym.service';
import { Rate } from "./rate.class";

@Component({
    moduleId: module.id,
    selector: 'tsa-rate-add',
    templateUrl: 'rate-add.component.html'
})

export class RateAddComponent implements OnInit {
  rateForm: FormGroup;
  rate: Rate;
  nameGym: string = '';
  gyms: Gym[];
  gym: Gym;
  starttime: Date;
  endtime: Date;
  weekDays: SelectItem[];
  weekdays: Array<any> = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
  selectedDays: string[] = [];

  constructor(private ratesService: RatesService,
              private gymsService: GymsService,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.weekDays = [];
    for(let i=0; i<7; i++){
      this.weekDays.push({label:this.weekdays[i],value:i});
    }
  }

  ngOnInit() {

    this.gymsService.gymsUpdated.subscribe(
      (gyms: Gym[]) => {
        this.gyms = gyms;
      }
    );
    this.gymsService.fetchGyms();
    this.gyms = this.gymsService.getGyms();


    this.rateForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      gymid: ['', Validators.required],
      training: [0, Validators.required],
      starttime: ['',Validators.required],
      endtime: ['',Validators.required],
      weekdays: ['',Validators.required],
      enddate: ['', Validators.required],
      count: [0, Validators.required],
    });
  }

  onAddGym(element: HTMLInputElement){
    this.gymsService.addGym(element.value);
    element.value = null;
  }

  onKeyUpGym(name:string){
    this.nameGym = name;
  }

  onCancel() {
    this.router.navigate(['/rates']);
  }

  onSubmit() {
    this.ratesService.addRate(this.rateForm.value);
    this.router.navigate(['/rates']);
  }
}
