import {Injectable, EventEmitter} from '@angular/core';
import {Rate} from "./rate.class";
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/Rx';
import appGlobals = require('../app.global'); //<==== config

@Injectable()
export class RatesService {
  rates: Rate[] = [];
  ratesUpdated = new EventEmitter<Rate[]>();
  headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('id_token')
  });

  constructor(private http: Http) {
  }

  getRates() {
    return this.rates;
  }

  getRateById(id: number) {
    for (var rate of this.rates) {
      if (rate.id === id) {
        return rate;
      }
    }
    return null;
  }

  deleteRate(rate: Rate) {
    const id = rate.id;

    this.rates.splice(this.rates.indexOf(rate), 1);
    this.http.delete(appGlobals.rest_server + 'rates/' + id, {headers: this.headers}).subscribe((res) => {
    });
  }

  editRate(oldRate: Rate, newRate: Rate) {
    this.rates[this.rates.indexOf(oldRate)] = newRate;
    newRate.starttime = this.getTime(new Date(newRate.starttime));
    newRate.endtime = this.getTime(new Date(newRate.endtime));
    const body = JSON.stringify(newRate);
    return this.http.put(appGlobals.rest_server + 'rates?nocache=' + new Date().getTime(), body, {headers: this.headers}).subscribe((res) => {
      this.ratesUpdated.emit(this.rates);
    });
  }

  addRate(rate: Rate) {
    rate.starttime = this.getTime(new Date(rate.starttime));
    rate.endtime = this.getTime(new Date(rate.endtime));
    const body = JSON.stringify(rate);

    return this.http.post(appGlobals.rest_server + 'rates?nocache' + new Date().getTime(), body, {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe((data) => {
        rate.id = data[0].id;
        this.rates.push(rate);
        this.ratesUpdated.emit(this.rates);
      });
  }

  fetchRates() {
    return this.http.get(appGlobals.rest_server + 'rates?nocache=' + new Date().getTime(), {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe(
        (data: Rate[]) => {
          this.rates = data;
          this.ratesUpdated.emit(this.rates);
        }
      );
  }
  twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
  }
  getTime(date){
    return this.twoDigits(date.getHours())+":"+this.twoDigits(date.getMinutes())+":00";
  }
}
