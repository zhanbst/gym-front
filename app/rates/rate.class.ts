export class Rate {
  constructor(public id: number,
              public name: string,
              public price: number,
              public gymid: number,
              public training: number,
              public starttime: any,
              public endtime: any,
              public weekdays: any,
              public enddate: number,
              public count: number
  ) {

  }
}
