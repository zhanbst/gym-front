import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'tsa-root',
  templateUrl: 'root.component.html'
})

export class RootComponent implements OnInit {


  constructor(private router: Router) {
  }

  ngOnInit() {

  }
}
