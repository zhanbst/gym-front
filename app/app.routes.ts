import {RouterModule} from '@angular/router';

import {InstructorsComponent} from "./instructors/instructors.component";
import {RootComponent} from './root/root.component';
import {InstructorEditComponent} from "./instructors/instructor-edit.component";
import {InstructorAddComponent} from "./instructors/instructor-add.component";
import {LoginComponent} from "./login/login.component";
import {LogoutComponent} from "./login/logout.component";
import {AuthGuardService} from "./auth-guard.service";
import {RateEditComponent} from "./rates/rate-edit.component";
import {RatesComponent} from "./rates/rates.component";
import {RateAddComponent} from "./rates/rate-add.component";
import {KeyAddComponent} from "./keys/key-add.component";
import {KeysComponent} from "./keys/keys.component";
import {KeyEditComponent} from "./keys/key-edit.component";
import {ClientEditComponent} from "./clients/client-edit.component";
import {ClientAddComponent} from "./clients/client-add.component";
import {ClientsComponent} from "./clients/clients.component";

export const appRoutes = [
  {path: '', redirectTo:'/registration', pathMatch: 'full'},
  {path: 'registration', component: RootComponent, canActivate: [AuthGuardService]},
  {path: 'instructors', component: InstructorsComponent, canActivate: [AuthGuardService]},
  {path: 'instructors/new', component: InstructorAddComponent, canActivate: [AuthGuardService]},
  {path: 'instructors/:id/edit', component: InstructorEditComponent, canActivate: [AuthGuardService]},
  {path: 'rates', component: RatesComponent, canActivate: [AuthGuardService]},
  {path: 'rates/new', component: RateAddComponent, canActivate: [AuthGuardService]},
  {path: 'rates/:id/edit', component: RateEditComponent, canActivate: [AuthGuardService]},
  {path: 'keys', component: KeysComponent, canActivate: [AuthGuardService]},
  {path: 'keys/new', component: KeyAddComponent, canActivate: [AuthGuardService]},
  {path: 'keys/:id/edit', component: KeyEditComponent, canActivate: [AuthGuardService]},
  {path: 'clients', component: ClientsComponent, canActivate: [AuthGuardService]},
  {path: 'clients/new', component: ClientAddComponent, canActivate: [AuthGuardService]},
  {path: 'clients/:id/edit', component: ClientEditComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent },
  {path: 'logout', component: LogoutComponent },

];

export const routing = RouterModule.forRoot(appRoutes);
