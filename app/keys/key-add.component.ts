import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { KeysService } from "./keys.service";
import { Key } from "./key.class";

@Component({
    moduleId: module.id,
    selector: 'tsa-key-add',
    templateUrl: 'key-add.component.html'
})

export class KeyAddComponent implements OnInit {
  keyForm: FormGroup;
  key: Key;

  constructor(private keysService: KeysService,
              private router: Router,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.keyForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  onCancel() {
    this.router.navigate(['/keys']);
  }

  onSubmit() {
    this.keysService.addKey(this.keyForm.value);
    this.router.navigate(['/keys']);
  }
}
