import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {KeysService} from "./keys.service";
import { Subscription } from 'rxjs/Rx';
import {Key} from "./key.class";

@Component({
    moduleId: module.id,
    selector: 'tsa-key-edit',
    templateUrl: 'key-edit.component.html'
})

export class KeyEditComponent implements OnInit, OnDestroy {
  keyForm: FormGroup;
  private subscription: Subscription;
  keyId: number;
  key: Key;

  constructor(private route: ActivatedRoute,
              private keysService: KeysService,
              private formBuilder: FormBuilder,
              private router: Router) {}

  ngOnInit() {
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.keyId = +params['id'];
          this.key = this.keysService.getKeyById(this.keyId);
        }
      }
    );
    this.initForm();
  }

  private initForm() {
    this.keyForm = this.formBuilder.group({
      id: [this.key.id, Validators.required],
      name: [this.key.name, Validators.required]
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onCancel() {
    this.router.navigate(['/keys']);
  }

  onSubmit() {
    const newKey = this.keyForm.value;
    this.keysService.editKey(this.key, newKey);
    this.router.navigate(['/keys']);
  }
}
