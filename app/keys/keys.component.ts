import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {KeysService} from "./keys.service";
import {Key} from "./key.class";

@Component({
  moduleId: module.id,
  selector: 'tsa-keys',
  templateUrl: 'keys.component.html'
})

export class KeysComponent implements OnInit {
  keys: Key[];

  constructor(public keysService: KeysService,
              private router: Router) {
  }

  ngOnInit() {
    this.keysService.keysUpdated.subscribe(
      (keys: Key[]) => {
        this.keys = keys;
      }
    );
    this.keys = this.keysService.getKeys();
    if (localStorage.getItem('id_token') == null) {
      this.router.navigate(['/login']);
    }
    else if (this.keys.length === 0) {
      this.keysService.fetchKeys();
    }

  }

  onDeleteKey(instructor: Key) {
    this.keysService.deleteKey(instructor);
    this.router.navigate(['/keys']);
  }

  onEditKey(instructor: Key) {
    this.router.navigate(['/keys', instructor.id, 'edit']);
  }

  onAddKey() {
    this.router.navigate(['/keys/new']);
  }
}
