import {Injectable, EventEmitter} from '@angular/core';
import {Key} from "./key.class";
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/Rx';
import appGlobals = require('../app.global'); //<==== config

@Injectable()
export class KeysService {
  keys: Key[] = [];
  keysUpdated = new EventEmitter<Key[]>();
  headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('id_token')
  });

  constructor(private http: Http) {
  }

  getKeys() {
    return this.keys;
  }

  getKeyById(id: number) {
    for (var instructor of this.keys) {
      if (instructor.id === id) {
        return instructor;
      }
    }
    return null;
  }

  deleteKey(instructor: Key) {
    const id = instructor.id;

    this.keys.splice(this.keys.indexOf(instructor), 1);
    this.http.delete(appGlobals.rest_server + 'keys/' + id, {headers: this.headers}).subscribe((res) => {
    });
  }

  editKey(oldKey: Key, newKey: Key) {
    this.keys[this.keys.indexOf(oldKey)] = newKey;
    const body = JSON.stringify(newKey);
    return this.http.put(appGlobals.rest_server + 'keys?nocache=' + new Date().getTime(), body, {headers: this.headers}).subscribe((res) => {
      this.keysUpdated.emit(this.keys);
    });
  }

  addKey(instructor: Key) {
    const body = JSON.stringify(instructor);
    return this.http.post(appGlobals.rest_server + 'keys?nocache' + new Date().getTime(), body, {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe((data) => {
        instructor.id = data[0].id;
        this.keys.push(instructor);
        this.keysUpdated.emit(this.keys);
      });
  }

  fetchKeys() {
    return this.http.get(appGlobals.rest_server + 'keys?nocache=' + new Date().getTime(), {headers: this.headers})
      .map((response: Response) => response.json())
      .subscribe(
        (data: Key[]) => {
          this.keys = data;
          this.keysUpdated.emit(this.keys);
        }
      );
  }
}
